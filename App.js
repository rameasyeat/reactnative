

import 'react-native-gesture-handler';
import React from 'react';
import SplashScreen from 'react-native-splash-screen';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import InitailScreen from './src/screen/InitailScreen';
import CountryList from './src/screen/CountryList';
import Webview from './src/screen/Webview';

import {Button} from 'react-native';

// import Webview from './screen/Webview';


const Stack = createStackNavigator();

  export default function App(){
  SplashScreen.hide();
  return (

    <NavigationContainer >
      <Stack.Navigator>
        {/* <Stack.Screen name="Movies List" component={InitailScreen}   options={{
          headerRight: () => (
            <Button
              onPress={() => alert('This is a button!')}
              title="Info"
              color="purple"
            />
          ),
        }}/>
        <Stack.Screen name="Country List" component={CountryList}/> */}

        <Stack.Screen  name='Webview' component={Webview} options={{
                headerShown: false
            }}/>

      </Stack.Navigator>


    </NavigationContainer>

  );
}


