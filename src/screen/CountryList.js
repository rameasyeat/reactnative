import React, { useEffect, useState } from 'react';
import { ActivityIndicator, FlatList, Text, View } from 'react-native';
import ListItems from '../compoenet/ListItem';
import CountryCodeListItem from '../compoenet/CountryCodeListItem';

const CountryList = ({navigation}) => {
  const [isLoading, setLoading] = useState(true);
  const [countryData, setCountryData] = useState([]);

  useEffect(() => {
    fetch('https://api-dev.easyeat.ai/user_login/get_countries_dial_code.php', {
        method: "POST",//Request Type 
      })
      .then((response) => response.json())
      .then((json) => {
          console.log(json);
          setCountryData(json.countries)
          
      })
      .catch((error) => console.error(error))
      .finally(() => setLoading(false));
  }, []);

const onClickItem = (name,dial_code) => {
    console.log(name,dial_code);
    navigation.pop();

}

  return (
    <View style={{ flex: 1, backgroundColor : 'white' }}>
      {isLoading ? <ActivityIndicator/> : (
    
        <FlatList
          data={countryData}
         keyExtractor={({ id }, index) => id}
          renderItem={({ item }) => <CountryCodeListItem item={item} onClickItem={onClickItem}/>}
        />
      )}
    </View>
  );
};

export default  CountryList;
