import React, { Component,useEffect } from 'react';
import { WebView } from 'react-native-webview';
import messaging from '@react-native-firebase/messaging';
import { Alert } from "react-native";
import { log } from 'react-native-reanimated';


export default function Webview() {

  useEffect(() => {
    requestUserPermission();
    const unsubscribe = messaging().onMessage(async remoteMessage => {
      console.log(remoteMessage);
      Alert.alert(remoteMessage.data.notification.title,remoteMessage.data.notification.title);
    });

    return unsubscribe;
   }, []);

   requestUserPermission = async () => {
    const authStatus = await messaging().requestPermission();
    const enabled =
      authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
      authStatus === messaging.AuthorizationStatus.PROVISIONAL;

    if (enabled) {
      getFcmToken() //<---- Add this
      console.log('Authorization status:', authStatus);
    }
  }

  getFcmToken = async () => {
    const fcmToken = await messaging().getToken();
    if (fcmToken) {
     console.log(fcmToken);
     console.log("Your Firebase Token is:", fcmToken);
    } else {
     console.log("Failed", "No token received");
    }
  }


    return (
        <WebView
        //originWhitelist={['*']}
        source={{ uri:"https://app-dev.easyeat.ai/index.php"}}
        style={{ marginTop: 20 }}
        
        javaScriptEnabled={true}
         domStorageEnabled={true}
          startInLoadingState={true}
      />
    );
  }
  
  handleWebViewNavigationStateChange = newNavState => {
    // newNavState looks something like this:
    // {
    //   url?: string;
    //   title?: string;
    //   loading?: boolean;
    //   canGoBack?: boolean;
    //   canGoForward?: boolean;
    // }
    const { url } = newNavState;
    if (!url) return;

    // handle certain doctypes
    if (url.includes('.pdf')) {
      this.webview.stopLoading();
      // open a modal with the PDF viewer
    }

    // one way to handle a successful form submit is via query strings
    if (url.includes('?message=success')) {
      this.webview.stopLoading();
      // maybe close this view?
    }

    // one way to handle errors is via query string
    if (url.includes('?errors=true')) {
      this.webview.stopLoading();
    }

    // redirect somewhere else
    if (url.includes('google.com')) {
      const newURL = 'https://easyeat.ai';
      const redirectTo = 'window.location = "' + newURL + '"';
      this.webview.injectJavaScript(redirectTo);
    }
  };


//  const Webview = () => {

//     <WebView
//         source={{
//           uri: 'https://easyeat.ai'
//         }}
//         style={{ marginTop: 20 }}
//       />
  
//  }

//  export default WebView;