import React, { useEffect, useState } from 'react';
import { ActivityIndicator, FlatList, Text, View ,TouchableOpacity} from 'react-native';
import ListItems from '../compoenet/ListItem';

const InitailScreen = ({navigation}) => {
  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState([]);

  useEffect(() => {
    fetch('https://reactnative.dev/movies.json')
      .then((response) => response.json())
      .then((json) => setData(json.movies))
      .catch((error) => console.error(error))
      .finally(() => setLoading(false));
  }, []);


  const itemAction = (title) => {
    navigation.navigate('Country List')
    console.log(title);
  }


  const getDataFromComponentB = (name,dial_code) => {
    console.log('ram ji',name);
    
  }

  return (
    <View style={{ flex: 1, backgroundColor : 'white' }}>
      {isLoading ? <ActivityIndicator/> : (

        
        <FlatList
          data={data}
          keyExtractor={({ id }, index) => id}
          renderItem={({ item }) => <ListItems item={item} itemAction={itemAction} />}
        />
      )}
    </View>
  );
};

export default  InitailScreen;
