
import React  from 'react';
import {Text,View,StyleSheet,TouchableOpacity,Image} from 'react-native';
import Icon from 'react-native-ionicons';





const ListItems = ({item,itemAction}) => {
    

  return(
    <TouchableOpacity
     style ={styles.listItem} 
     onPress={() => itemAction(item.title)}
     >
     <View style={styles.listItemView}>
         <Text style={styles.listItemText}> {item.title}</Text>
         <Image source={require('../images/forword.png')} style={{width : 7, height:10}}  />

     </View>

    </TouchableOpacity>

  );

}


const styles = StyleSheet.create({
  listItem : {
    backgroundColor: 'white',
  },
   listItemText :{
    color : 'black',
    fontSize : 14,
    textAlign : 'center',
  },
  listItemView : {
      flexDirection: 'row',
      justifyContent : 'space-between',
      alignItems:'center',
      padding : 15,
      margin : 5,
      marginLeft : 16,
      marginRight : 16,
      marginTop : 16,
      borderRadius : 8,
      backgroundColor : 'white',
      shadowColor: "#000000",
      shadowOpacity: 0.6,
      shadowRadius: 2,
      shadowOffset: {
        height: 1,
        width: 0
      }
  }
 

});

export default ListItems;