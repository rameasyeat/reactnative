
import React  from 'react';
import {Text,View,StyleSheet,TouchableOpacity,Image} from 'react-native';
import Icon from 'react-native-ionicons';

const CountryCodeListItem = ({item,onClickItem}) => {
    
  return(
    <TouchableOpacity
     style ={styles.listItem} 
     onPress={() => onClickItem(item.name,item.dial_code)}
     >
     <View style={styles.listItemView}>
         <Image source={{uri:item.icon_s3}} style={{width : 40, height:20, marginEnd : 10}}  />

         <View>
         <Text style={styles.listItemText}> {item.name}</Text>
         </View>
         
         <View style={{flex : 1}}>
         <Text style={styles.dialCode}> {item.dial_code}</Text>
         </View>
        


     </View>

    </TouchableOpacity>

  );

}


const styles = StyleSheet.create({
  listItem : {
    backgroundColor: 'white',
  },
   listItemText :{
    color : 'black',
    fontSize : 14,
    // backgroundColor : 'red',
  },

  dialCode :{
    color : 'black',
    fontSize : 14,
    textAlign: 'right',
    // backgroundColor : 'purple',
    
  },
  listItemView : {
      flexDirection: 'row',
      justifyContent :'flex-start',
      alignItems:'center',
      padding : 15,
      marginLeft : 16,
      marginRight : 16,
      backgroundColor : 'white',
      borderBottomWidth : 1,
      borderBottomColor : 'lightgray',
  }
 

});

export default CountryCodeListItem;